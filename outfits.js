const outfits = [
  {
    degrees: 80,
    imageUrl: 'assets/tank.svg',
    description: 'Sun\'s out, guns out.  Grab that tank top!' 
  },
  {
    degrees: 70,
    imageUrl: 'assets/t-shirt.svg',
    description: 'It\'s t-shirt time!  Enjoy the nice day'
  },
  {
    degrees: 60,
    imageUrl: 'assets/sweatshirt.svg',
    description: 'Keep it cozy, toss on that sweatshirt'
  },
  {
    degrees: 50,
    imageUrl: 'assets/jacket.svg',
    description: 'Looking a bit chilly, put on a jacket.'
  },
  {
    degrees: -100,
    imageUrl: 'assets/heavy-jacket.svg',
    description: 'Brrr... it\'s cold!  Better wear a puffy jacket'
  }
]